; Insert future code HERE! i.e. constants, variables, functions

; Scancode for Escape Key is 1
; "Globalizing" is good for commonly used values 
Global escape_key = 1
Global screen_width = 640, screen_height = 480

; Set video mode 
Graphics3D screen_width, screen_height, 16, 2 	
SetBuffer BackBuffer()

; Setup camera 
cam1 = CreateCamera()
CameraViewport cam1, 0, 0, screen_width, screen_height

; Load level objects, e.g. trees, rocks, enemies, lights, etc
light1 = CreateLight( 2 ) ; 2 = Point Light Type
cube1 = CreateCube()
PositionEntity cube1 ,-2, -2, 10

; The MAIN GAME LOOP Keeps repeating While you do NOT press ESCape

While Not KeyHit( escape_key )
     ; Keyboard & Mouse Controls will be here 
     ; 3D Object Changes Go Here, e.g. move/animate objects, shoot, check collisions...

     UpdateWorld 
     RenderWorld

     ; 2D stuff here, update HUD, text, stats...
     Text 50,50,"Hello Cube!"

     Flip
Wend

End

; Repeatedly used functions appear here later